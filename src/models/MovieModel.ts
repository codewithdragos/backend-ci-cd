import mongoose = require("mongoose");

const MovieSchema = new mongoose.Schema({
    id: {
        type: String,
        unique: true,
        null: false
    },
    title: String,
    year: Number,
    genre: {
      type: [String],
      default: []
    },
  })

const Movie = mongoose.model("MovieNewCollection", MovieSchema);

export default Movie;