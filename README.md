# Action Item: Software Lifecycle - Backend

In this Action Item we will deploy the Backend REST API we built in the previous week. If you haven't already, we recommend you complete the material on `Fullstack Thinking` before this one. The knowledge here, builds on the previous material.

**Make sure you set aside at least 2 hours of focused, uninterrupted work and give your best.**

In order to deploy the backend application we will:

1. Get an account on `GitLab`
2. Get an account on `AWS`
3. Create a `GitLab` repository for the backend app
4. Create a virtual server in `EC2` and deploy manually

- use a short video here

5. Automatize the deployment by creating a pipeline in `Gitlab`

- add the pipeline
- add the deploy script
- add the environment variables --> short video here

## App Setup:

Install dependencies:

```bash
npm install
```

Run in production mode:

```bash
npm run production
```

---

### Preparation

1. Add a `.env` file
2. Build the code
3. Run the app with the `start` script and check is working

---

## 1. Get an account on `GitLab`

If you do not have one already, check the [SUPPORT.md](SUPPORT.md) file in this repository.

---

## 2. Get an account on `AWS`

If you do not have one already, check the [SUPPORT.md](SUPPORT.md) file in this repository.

---

## 3. Create a `GitLab` repository for the backend app

Check the [SUPPORT.md](SUPPORT.md) file in this repository for step by step instructions.

---

## 4. Create a virtual server in `EC2`, install `node.js` and deploy manually

4.1 Create a `linux` virtual server on `EC2`

-- link video here

4.2 Connect to the virtual server on `EC2` with `ssh`, install node.js and upload the files

4.3 Upload the code with `scp` and deploy manually

```bash
chmod 400 AWS_KEY
# copy files to the ec2 instance
scp -i AWS_KEY -r build ec2-user@INSTANCE_IP:/home/ec2-user/build
scp -i AWS_KEY package.json ec2-user@INSTANCE_IP:/home/ec2-user/build/package.json
```

4.4 Create a `.env` file in the `AWS` instance
Run the `nano` editor in the `terminal` of the instance, inside the build folder:

```bash
nano .env
```

And add inside the content of your `.env` file.

4.5 Install `pm2` on the `instance` and run your app:

```bash
npm install pm2 -g
# in the ./build folder on the virtual machine -- instance
pm2 start index.js -i max
```

4.6 Check the application is now live on the instance:

Go to [http://INSTANCE_IP/api-docs/](http://INSTANCE_IP/api-docs/)

---

## 5. Automatize the deployment by creating a pipeline in `Gitlab`

We have been able to deploy manually, but is very time consuming and error prone.

Let's automate the deployment using `Gitlab`.

---

### 1. Go to pipelines:

![Create Pipeline Step 1](examples/add_pipeline_step_one.png)

---

### 2. Click --> Create New Pipeline && remove the default code:

![Create Pipeline Step 2](examples/add_pipeline_step_two.png)

---

### 3. Add the following code instead:

```yaml
image: node:latest
stages: ["build", "test", "deploy"]

# WARNING
# This pipeline needs the following variables set up to work:
# INSTANCE_IP =  // the public IP of the AWS instance
# SECRET_KEY = // the secret key to connect to the AWS instance (.pem) file
# ENV_FILE = // the .env file for production

# the build job
build:
  stage: build
  script:
    - npm install
    - npm run lint
    - npm run build
  artifacts:
    paths:
      - build
    expire_in: 1 week

# the test job
test:
  stage: test
  variables:
    MONGOMS_VERSION: 4.2.8
    MONGOMS_DEBUG: 1
    MONGOMS_DOWNLOAD_URL: https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-ubuntu1804-4.2.8.tgz
  script:
    - npm install
    - npm run test
  artifacts:
    paths:
      - coverage
    expire_in: 1 week

deploy:
  stage: deploy
  only:
    - main
  before_script: # prepare the pipeline runner for deploy by installing ssh
    - "which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )"
    - eval $(ssh-agent -s)
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
    - apt-get update -y
    - apt-get -y install rsync
  script:
    - chmod 400 $SECRET_KEY
    # clean up the ec32 instance
    - ssh -i $SECRET_KEY ec2-user@$INSTANCE_IP 'rm -rf /home/ec2-user/api'
    - ssh -i $SECRET_KEY ec2-user@$INSTANCE_IP 'mkdir /home/ec2-user/api'
    
    # copy files to the ec2 instance
    - scp -i $SECRET_KEY -r build ec2-user@$INSTANCE_IP:/home/ec2-user/api/build
    - scp -i $SECRET_KEY package.json ec2-user@$INSTANCE_IP:/home/ec2-user/api/package.json
    - scp -i $SECRET_KEY deploy.sh ec2-user@$INSTANCE_IP:/home/ec2-user/api/deploy.sh
    
    # copy .env to the ec2 instance
    - scp -i $SECRET_KEY $ENV_FILE ec2-user@$INSTANCE_IP:/home/ec2-user/api/build/.env

    # run the deploy script
    - ssh -i $SECRET_KEY ec2-user@$INSTANCE_IP 'cd /home/ec2-user/api && bash deploy.sh'
  needs:
    - job: build
      artifacts: true
  dependencies:
    - build
```

---

5.3 Add a `deploy.sh` script to be executed by Gitlab in the deployment job:

Create the `deploy.sh` file in the root folder of the repository:
```bash
touch deploy.sh
```

An add to it the following
```
# clean up old app
pm2 kill
npm remove pm2 -g

# installing dependencies
echo "Installing npm2"
npm install pm2 -g
echo "Running npm install"
npm install

# start app with pm2
echo "Running the app"
npm run start:production
```

5.3 Add the production and monitor script to `package.json`:

```diff
  "scripts": {
    "lint": "eslint --fix",
    "test": "jest --coverage",
    "build": "rm -rf build && tsc && npm run copy-files",
    "start": "node build/index.js",
+   "start:production": "pm2 start build/index.js --name 'api' -f",
+   "monit": "pm2 monit",
    "dev": "nodemon src/index.ts",
    "copy-files": "copyfiles -u 1 src/**/*.yaml build/"
  },
```


5.3 Add the missing variables the `ci-cd` file needs: INSTANCE_IP, SECRET_KEY and ENV_FILE

- [x] add `INSTANCE_IP` to the `Gitlab` variables
![Add INSTANCE_IP](examples/add_instance_ip_variable.png)

----

- [x] add `SECRET_KEY` to the `Gitlab` variables
![Add SECRET_KE](examples/add_secret_key_variable.png)

----

- [x] add `ENV_FILE` to the `Gitlab` variables
![Add ENV_FILE](examples/add_env_file_variable.png)

5.6 Push your code:
```bash
git push
```

5.7 Run the pipeline:
![Run the pipeline](examples/add_pipeline_step_sixteen.png)


---

## Wrapping up

1. Make sure you push and commit your code
2. Add tests and documentation for all the endpoints
3. Add sequence diagrams
4. Keep adding new features, for example: you can use libraries like `joi` for validation

### Made with :orange_heart: in Berlin by @CodeWithDragos
